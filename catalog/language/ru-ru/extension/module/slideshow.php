<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 20.08.2020
 * Time: 07:42
 */
$_['select_part_title'] = 'Выберите Вашу машину для поиска запчастей';
$_['select_brand']      = 'Выберите марку';
$_['select_brand_label']= 'Марка';
$_['select_model']      = 'Выберите модель';
$_['select_model_label']= 'Модель';
$_['select_engin']      = 'Выберите двигатель';
$_['select_engin_label']= 'Двигатель';
$_['license_plate_label']     = 'Гос.номер';
