<?php

/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 22.07.2020
 * Time: 19:07
 */
class ModelAutosellingAutosellingserver extends Model
{

    /* ������� ���������� �������� �� ������
     * */

    public function getRequestToServer($params){
        $url = $params['url'];
        if (empty($params['token']) && strrpos($params['url'], 'get_access_token') === false){
            return [];
        }
        $headers = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => "Pragma: no-cache\r\n".
                    "Cache-Control: no-cache\r\n".
                    "Content-Type: application/x-www-form-urlencoded\r\n".
                    "Origin: ".$this->config->get('config_origin')."\r\n",
                'content' => http_build_query($params, '', '&'),
            ),
        ));
        try {
            $result = json_decode(file_get_contents($url, false, $headers));
            if(empty($result)){
                return [];
            }
            if(isset($result->success) && !$result->success && empty($result->token)){
                $this->getAccessToken();
                $result = json_decode(file_get_contents($url, false, $headers));
            }else{
                foreach ($http_response_header as $item){
                    if(strpos($item, 'AUTOPRODAZH-API-TOKEN') !== false){
                        $token_param = explode(':', $item);
                        if($token_param[0] == 'AUTOPRODAZH-API-TOKEN') {
                            if(is_object($result)) {
                                $result->token = trim($token_param[1]);
                            }
                        }
                    }
                }
            }
            $session = $this->registry->get('session');
            unset($session->data['autoprodag_token']);
            if(isset($result->success) && $result->success) {
                $session->data['autoselling_token'] = $result->token;
            }
            $this->registry->set('session', $session);
            return $result;
        }catch (Exception $exception){
            return [];
        }
    }

    /* ����������� ���������� �����
     * */
    public function getAccessToken(){
        $session = $this->registry->get('session');
        unset($session->data['autoprodag_token']);
        $params = [
            'url' => 'https://autoselling.eu/api/auth/get_access_token.php',
            'id_key' => $this->config->get('config_token')
        ];
        $result = $this->getRequestToServer($params);
        if(!empty($result) && $result->success){
            $session->data['autoselling_token'] = $result->token;
            $this->registry->set('session', $session);
        }
        return $result;
    }

    public function searchArticlesSuggest($search_params){
        if(!isset($this->registry->get('session')->data['autoselling_token']) || empty($this->registry->get('session')->data['autoselling_token'])){
            $this->getAccessToken();
        }
        $params = [
            'url' => 'https://autoselling.eu/api/autosales/search_articles_suggest.php',
            'token' => $this->registry->get('session')->data['autoselling_token'],
        ];
        foreach ($search_params as $key=>$param){
            $params[$key] = $param;
        }
        $result = $this->getRequestToServer($params);
        if(!$result->success){
            switch ($result->info){
                case 'invalid access token':{
                    $this->session->data['autoselling_token'] = $this->getAccessToken();
                }break;
            }
            return $this->getRequestToServer($params);
        }
        if ($result->data->suggests_list !== null){
            foreach ($result->data->suggests_list as $key=>$item){
                $result->data->suggests_list[$key] = $this->convertObjectToArray($item);
            }
            return $result->data->suggests_list;
        }else{
            return [];
        }
    }
    /*Поиск автомобиля по регистрационному номеру
     * */
    public function getTdvehicleSuggest($license_plate){
        if(!isset($this->registry->get('session')->data['autoselling_token']) || empty($this->registry->get('session')->data['autoselling_token'])){
            $this->getAccessToken();
        }
        $params = [
            'url' => 'https://autoselling.eu/api/autosales/get_tdvehicle_suggest.php',
            'token' => $this->registry->get('session')->data['autoselling_token'],
            'license_plate' => $license_plate,
        ];
        $result = $this->getRequestToServer($params);
        if(isset($result->success) && $result->success){
            return json_encode($result->data->nodes);
        }else{
            return '[]';
        }
    }

    /*Получаю список всех основных узлов
     * */
    public function getTdvehicleSections($parent_vehicle_id = '', $parent_section_id = 0){
        if(!isset($this->registry->get('session')->data['autoselling_token']) || empty($this->registry->get('session')->data['autoselling_token'])){
            $this->getAccessToken();
        }
        $params = [
            'url' => 'https://autoselling.eu/api/autosales/get_tdvehicle_sections.php',
            'parent_vehicle_id' => $parent_vehicle_id,
            'token' => $this->registry->get('session')->data['autoselling_token'],
        ];
        if($parent_section_id){//Если указан ИД родительской секции
//            unset($params['parent_vehicle_id']);
            $params['parent_section_id'] = $parent_section_id;
        }
        $result = $this->getRequestToServer($params);
        if(isset($result->success) && $result->success) {
            return $result->data->sections;
        }else{
            return [];
        }
    }
    /*Получаю список всех марок легковых авто
     * */
    public function getBrands(){
        if(!isset($this->registry->get('session')->data['autoselling_token']) || empty($this->registry->get('session')->data['autoselling_token'])){
            $this->getAccessToken();
        }
        $params = [
            'url' => 'https://autoselling.eu/api/autosales/get_tdvehicle_nodes.php',
            'token' => $this->registry->get('session')->data['autoselling_token'],
            'parent_id' => !isset($_REQUEST['brand_id'])||empty($_REQUEST['brand_id'])?2:$_REQUEST['brand_id'],//Легковые автомобили
        ];
        $out = [];
        $result = $this->getRequestToServer($params);
        if(isset($result->success) && $result->success){
            if($result->data->nodes[0]->level == 1){
                $out['brands'] = $result->data->nodes;
                $params['parent_id'] = $out['brands'][0]->id;
                $result = $this->getRequestToServer($params);
            }
            if ($result->data->nodes[0]->level == 2){
                if (isset($result->data)) {
                    $out['model'] = $result->data->nodes;
                    $params['parent_id'] = $out['model'][0]->id;
                    $result = $this->getRequestToServer($params);
                }
            }
            if ($result->data->nodes[0]->level == 3){
                if (isset($result->data)) {
                    $out['engine'] = $result->data->nodes;
                }
            }
            return json_encode($out);
        }else{
            return '[]';
        }
    }

    /*����� �� ������������ ��� ��������
     * */
    public function searchArticles($search_params){
        if(!isset($this->registry->get('session')->data['autoselling_token']) || empty($this->registry->get('session')->data['autoselling_token'])){
            $this->getAccessToken();
        }
        $params = [
            'url' => 'https://autoselling.eu/api/autosales/search_articles.php',
            'token' => $this->registry->get('session')->data['autoselling_token'],
        ];
        foreach ($search_params as $key=>$param){
            $params[$key] = $param;
        }
        $result = $this->getRequestToServer($params);
        if(!$result->success){
            switch ($result->info){
                case 'invalid access token':{
                    $this->session->data['autoselling_token'] = $this->getAccessToken();
                }break;
            }
            return $this->getRequestToServer($params);
        }
        if ($result->data->articles_list !== null){
            foreach ($result->data->articles_list as $key=>$item){
                $result->data->articles_list[$key] = $this->convertObjectToArray($item);
            }
            return $result->data->articles_list;
        }else{
            return [];
        }
    }

    /*������� ������ �������� ���������
     * */
    public function getCategories($parent_id = 0){
        if(!isset($this->registry->get('session')->data['autoselling_token']) || empty($this->registry->get('session')->data['autoselling_token'])){
            $this->getAccessToken();
        }
        $params = [
            'url'           => 'https://autoselling.eu/api/autosales/get_itemgroups_tree.php',
            'parent_id'     => $parent_id,
            'token'         => $this->registry->get('session')->data['autoselling_token'],
        ];
        $result = $this->getRequestToServer($params);
        $out = [];
        if(isset($result->data)){
            foreach ($result->data as $key=>$category){
                $out[] = $this->convertGroupInfoToArray($category);
            }
        }
        return $out;
    }

    /*������� ���������� � ���������
     * */
    public function getCategory($category_id){
        $categories = $this->getCategories();
        return $this->foreachCategories($categories, $category_id);
    }

    /*��������� ������ � ������� ������������
     * */
    public function setUserCredentials($quest_info){
        $params = [
            'url'       => 'https://autoselling.eu/api/autosales/set_user_credentials.php',
            'user_json' => json_encode($quest_info),
            'token'         => $this->registry->get('session')->data['autoselling_token'],
        ];
        return $this->getRequestToServer($params);
    }

    /*��������� �������� ������
     * */
    public function setShippingData($info){
        $params = [
            'url'   => 'https://autoselling.eu/api/autosales/set_shipping_data.php',
            'ship_data_js' => json_encode($info),
            'token'         => $this->registry->get('session')->data['autoselling_token'],
        ];
        $result = $this->getRequestToServer($params);
        return $result;
    }

    /*��������� ������
     * */
    private function foreachCategories($categories, $category_id){
        foreach ($categories as $category){
            //���� ������� ������� - ������� ���������
            if($category['category_id'] == $category_id){
                return $category;
            }
            //���� ��������� �������� �������� ��������� - �������� � ���
            if(count($category['children']) > 0) {
                $result = $this->foreachCategories($category['children'], $category_id);
                if($result && $result['category_id'] == $category_id){
                    if($result['properties'] && isset($_REQUEST['propvalue'])){
                        foreach ($result['properties'] as $key   =>  $property){
                            foreach ($property->values as $propvalue_idex =>$value){
                                if (strrpos($_REQUEST['propvalue'], strval($value->value_id)) !== false) {
                                    $value->checked = true;
                                }
                            }
                        }
                    }
                    return $result;
                }
            }
        }
        return null;
    }


    /* ������� ���������� � ��������
     * */
    public function getProduct($united_id) {
        if(!isset($this->registry->get('session')->data['autoprodag_token']) && (!isset($this->registry->get('session')->data['autoselling_token']) || empty($this->registry->get('session')->data['autoselling_token']))){
            $this->getAccessToken();
        }
        $params = [
            'url'           => 'https://autoselling.eu/api/autosales/search_articles.php',
            'united_ids'    => $united_id,
            'token'         => $this->registry->get('session')->data['autoselling_token'],
        ];
        $result = $this->getRequestToServer($params);
        if($result != null){
            $result = $result->data->articles_list;
        }
        $query = [];
        if(is_array($result)) {
            foreach ($result as $product) {
                if ($product->united_id == $united_id) {
                    $query = $product;
                    break;
                }
            }
        }
        if ($query) {

            $tech_data = "";
            foreach ($query->tech_data as $data){
                $tech_data .= $data->property_name ." ". $data->property_value."<br>";
            }
            $out = $this->convertObjectToArray($query);
            $out['description'] .= PHP_EOL.$tech_data;
            return $out;
        } else {
            return false;
        }
    }

    /*�������� ���� 1 ������� ������� �������
     * */
    public function increaseQtyCart($united_product_id){
        $params = [
            'url'               => 'https://autoselling.eu/api/autosales/cart_qty_increase.php',
            'united_product_id' => $united_product_id,
            'token'             =>
                isset($this->registry->get('session')->data['autoprodag_token']) && $this->registry->get('session')->data['autoprodag_token']?
                    $this->registry->get('session')->data['autoprodag_token']:
                    $this->registry->get('session')->data['autoselling_token'],
        ];
        return $this->getRequestToServer($params);
    }

    /*������ ����� 1 ������� ������� �������
     * */
    public function decreaseQtyCart($united_product_id){
        $params = [
            'url'               => 'https://autoselling.eu/api/autosales/cart_qty_decrease.php',
            'united_product_id' => $united_product_id,
            'token'             =>
                isset($this->registry->get('session')->data['autoprodag_token']) && $this->registry->get('session')->data['autoprodag_token']?
                    $this->registry->get('session')->data['autoprodag_token']:
                    $this->registry->get('session')->data['autoselling_token'],
        ];
        return $this->getRequestToServer($params);
    }

    /*����������� ������, �������� ������ � ������
     * */

    private function convertObjectToArray($object){
        return array(
            'product_id'       => $object->united_id,
            'name'             => $object->name,
            'description'      => $object->description??'',
            'meta_title'       => '',
            'meta_description' => '',
            'meta_keyword'     => '',
            'tag'              => '',
            'model'            => $object->articul,
            'articul'          => $object->articul,
            'sku'              => '',
            'upc'              => '',
            'ean'              => '',
            'jan'              => '',
            'isbn'             => '',
            'mpn'              => '',
            'location'         => '',
            'you_save'         => false,
            'quantity'         => 10000,
            'stock_status'     => 6,//������� ��� �������� �� ����������
            'image'            => $object->article_img??'',
            'manufacturer_id'  => '',
            'manufacturer'     => $object->brand,
            'price'            => $object->best_price_order??'',
            'special'          => 0,
            'reward'           => 0,
            'points'           => 1000,
            'tax_class_id'     => 0,
            'date_available'   => date('dd.mm.yyyy'),
            'weight'           => '',
            'weight_class_id'  => '',
            'length'           => '',
            'width'            => '',
            'height'           => '',
            'length_class_id'  => '',
            'subtract'         => '',
            'united_product_id'=> isset($object->price_stock_info)?$object->price_stock_info[0]->united_product_id:'',
            'rating'           => $object->sale_rate??0,
            'reviews'          => 0,
            'minimum'          => 0,
            'sort_order'       => 0,
            'status'           => 1,
            'date_added'       => date('dd.mm.yyyy'),
            'date_modified'    => date('dd.mm.yyyy'),
            'viewed'           => 0
        );
    }

    /* ������� ������ �������
     * */
    public function getProducts($category_id, $aditional_params = []){
        $params = [
            'url'               => 'https://autoselling.eu/api/autosales/search_articles.php',
            'group_id'          => $category_id,
            'token'             =>
                isset($this->registry->get('session')->data['autoprodag_token']) && $this->registry->get('session')->data['autoprodag_token']?
                    $this->registry->get('session')->data['autoprodag_token']:
                    $this->registry->get('session')->data['autoselling_token'],
        ];
        foreach ($aditional_params as $key=>$param){
            $params[$key] = $param;
        }
        $result = $this->getRequestToServer($params);
        if (isset($result->data->articles_list)) {
            return $result->data->articles_list;
        }else{
            return [];
        }
    }

    /*�������� ����� � ������� ����������
     * */
    public function addToCart($product_info, $quantity){
        for ($i = 1; $i <= $quantity; $i++){
            $params = [
                'url'               => 'https://autoselling.eu/api/autosales/add_item_to_cart.php',
                'united_product_id' => $product_info['united_product_id'],
                'token'             =>
                    isset($this->registry->get('session')->data['autoprodag_token']) && $this->registry->get('session')->data['autoprodag_token']?
                        $this->registry->get('session')->data['autoprodag_token']:
                        $this->registry->get('session')->data['autoselling_token'],
            ];
            $result = $this->getRequestToServer($params);
            if(!isset($this->registry->get('session')->data['autoprodag_token'])){
                $session = $this->registry->get('session');
                $session->data['autoprodag_token'] = $result->token;
                $this->registry->set('session', $session);
            }
        }
    }

    /*������ ������ �� �������*/
    public function removeFromCart($united_product_id){
        $params = [
            'url'               => 'https://autoselling.eu/api/autosales/cart_remove_item.php',
            'united_product_id' => $united_product_id,
            'token'             =>
                isset($this->registry->get('session')->data['autoprodag_token']) && $this->registry->get('session')->data['autoprodag_token']?
                    $this->registry->get('session')->data['autoprodag_token']:
                    $this->registry->get('session')->data['autoselling_token'],
        ];
        return $this->getRequestToServer($params);
    }

    /*������� ���������� � ���������� ������ �������
     * */
    public function getCartContent(){
        if(!isset($this->registry->get('session')->data['autoprodag_token']) && !isset($this->registry->get('session')->data['autoselling_token'])){
            $this->getAccessToken();
        }
        $params = [
            'url'               => 'https://autoselling.eu/api/autosales/get_cart_contents.php',
            'token'             =>
                trim(isset($this->registry->get('session')->data['autoprodag_token']) && $this->registry->get('session')->data['autoprodag_token']?
                    $this->registry->get('session')->data['autoprodag_token']:
                    $this->registry->get('session')->data['autoselling_token']),
        ];
        $result = $this->getRequestToServer($params);
        if(isset($result->data)){
            return $result->data;
        }else{
            return new stdClass();
        }
    }

    /*���������� ����� � ���������
     * */
    public function cartCheckIn(){
        $params = [
            'url'               => 'https://autoselling.eu/api/autosales/cart_check_in.php',
            'token'             =>
                isset($this->registry->get('session')->data['autoprodag_token']) && $this->registry->get('session')->data['autoprodag_token']?
                    $this->registry->get('session')->data['autoprodag_token']:
                    $this->registry->get('session')->data['autoselling_token'],
        ];
        $result = $this->getRequestToServer($params);
        return $result;
    }

    /*�������� ������ ���������� � ������ �������
     * */
    private function convertGroupInfoToArray($info){
        $children = [];
        foreach ($info->children as $key=>$child){
            $children[$key] = $this->convertGroupInfoToArray($child);
        }

        return [
            'category_id' => $info->group_id,
            'parent_id'   => $info->parent_id,
            'top'         => !$info->parent_id,
            'name'        => $info->group_name,
            'column'      => 1,
            'status'      => 1,
            'sort_order'  => 1,
            'page_group_links' =>'',
            'image' =>'',
            'description'=>'',
            'meta_description'=>'',
            'meta_keyword'=> '',
            'meta_title'  => $info->group_name,
            'store_id'    => 0,
            'children'    => $children,
            'properties'  => $info->itemprops,
            'propvalue'   => isset($_REQUEST['propvalue'])?$_REQUEST['propvalue']:'',
        ];
    }
}